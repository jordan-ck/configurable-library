defmodule ConfigurableLibrary do
  @moduledoc """
  Documentation for ConfigurableLibrary.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ConfigurableLibrary.hello
      :world

  """
  def hello do
    :world
  end

  def value1() do
    Application.get_env(:configurable_library, :value1)
  end

  def value2() do
    Application.get_env(:configurable_library, :value2)
  end

  def value3() do
    Application.get_env(:configurable_library, :value3)
  end
end
