defmodule ConfigurableLibraryTest do
  use ExUnit.Case
  doctest ConfigurableLibrary

  test "value1 test" do
    assert 3 == ConfigurableLibrary.value1()
  end

  test "value2 test" do
    assert 2 == ConfigurableLibrary.value2()
  end

  test "value3 test" do
    assert 42 == ConfigurableLibrary.value3()
  end
end
